<?php
require __DIR__ . '/../vendor/autoload.php';

use App\Models\Recipes;
use App\Controllers\RecipeController;

// dotenv
$dotenv = Dotenv\Dotenv::create(__DIR__ . '/../');
$dotenv->load();

$klein = new \Klein\Klein();

$klein->respond(function ($request, $response, $service, $app) use ($klein) {
    // Handle exceptions => flash the message and redirect to the referrer
    $klein->onError(function ($klein, $err_msg) {
        $klein->service()->flash($err_msg);
        $klein->service()->back();
    });

    try {
        // connection to database
        $app->db = new PDO('mysql:host='.$_ENV['DB_RECIPE_HOST'].';port='.$_ENV['DB_RECIPE_PORT'].';dbname='.$_ENV['DB_RECIPE_NAME'], $_ENV['DB_RECIPE_USER'], $_ENV['DB_RECIPE_PSSW']);
        $app->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);

        $app->controller = new RecipeController($app->db);
        $app->recipe = new Recipes;
    } catch(PDOException $e) {
        die($e->getMessage());
    }
});

$klein->respond('GET', '/', function ($request, $response, $service, $app) {
    return $service->render(__DIR__ . '/templates/index.html');
});


$klein->respond('GET', '/api/get-recipes', function ($request, $response, $service, $app) {
    $send = $request->param('format', 'json');
    $response->$send($app->controller->default());
    return $response;
});

$klein->respond('POST', '/api/create-recipe', function ($request, $response, $service, $app) {
    $app->recipe->setRecipe($request->body());

    $send = $request->param('format', 'json');
    $response->$send($app->controller->create($app->recipe));
    return $response;
});

$klein->respond('GET', '/api/get-recipe/[:id]', function ($request, $response, $service, $app) {
    $send = $request->param('format', 'json');
    $response->$send($app->controller->read($request->paramsNamed()->id));
    return $response;
});

$klein->respond('PUT', '/api/update-recipe/[:id]', function ($request, $response, $service, $app) {
    $app->recipe->setRecipe($request->body());
    $send = $request->param('format', 'json');
    $response->$send($app->controller->update($app->recipe));
    return $response;
});

$klein->respond('DELETE', '/api/delete-recipe/[:id]', function ($request, $response, $service, $app) {
    $send = $request->param('format', 'json');
    $response->$send($app->controller->delete($request->paramsNamed()->id));
    return $response;
});

$klein->dispatch();