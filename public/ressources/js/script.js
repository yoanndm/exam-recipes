const requests = axios.create({
    baseURL: window.location.href + 'api',
    timeout: 1500,
})

new Vue({
    // delimiters: ['${', '}'],
    el: '#vue-engine',
    data: {
        message: "",
        recipe: "",
        ingredient: "",
        step: "",
        notification: {
            addRecipe: {
                "open": false,
                "type": "",
                "message": ""
            },
            deleteRecipe: {
                "open": false,
                "type": "",
                "message": ""
            }
        },
        recipes: [],
        ingredients : [],
        steps : [],
        recipeTitle: "",
        recipeDescription: "",
        recipePreparationTime: 1,
        recipeServingAmount: 1,
        buttonDefault: true,
        buttonUpdate: false
    },
    methods: {
        getRecipe: function(id) {
            t = this
            requests.get('/get-recipe/' + id).then(function(response) {
                t.recipe = response.data.content
            })

            if(this.buttonUpdate == true) {
                this.buttonDefault = true
                this.buttonUpdate = false
                this.recipeTitle = ""
                this.recipeDescription = ""
                this.recipePreparationTime = 1
                this.recipeServingAmount = 1
                this.ingredients = ""
                this.steps = ""
            }
        },
        close_event: function() {
            this.notification.addRecipe.open = false
            this.notification.addRecipe.type = ""
            this.notification.addRecipe.message = ""
        },
        close_event2: function() {
            this.notification.deleteRecipe.open = false
            this.notification.deleteRecipe.type = ""
            this.notification.deleteRecipe.message = ""
        },
        addIngredient: function() {
            if(this.ingredient) {
                this.ingredients.push({id: Date.now(), title: this.ingredient})
                this.ingredient= ""
            }
        },
        addStep: function() {
            if(this.step) {
                this.steps.push({id: Date.now(), title: this.step})
                this.step= ""
            }
        },
        addRecipe: function() {
            t = this
            requests.post('/create-recipe', {
                'recipeTitle': this.recipeTitle,
                'recipeDescription': this.recipeDescription,
                'recipePreparationTime': this.recipePreparationTime,
                'recipeServingAmount': this.recipeServingAmount,
                'ingredients': this.ingredients,
                'steps': this.steps,
            }).then(function(response) {
                console.log(response)
                res = response.data.content
                if(res) {
                    t.recipe = res
                    t.recipes.push({id: res.id, title: res.title})
                    t.recipeTitle = ""
                    t.recipeDescription = ""
                    t.recipePreparationTime = 1
                    t.recipeServingAmount = 1
                    t.steps = []
                    t.ingredients = []

                    t.notification.addRecipe.open = true
                    t.notification.addRecipe.type = "is-success"
                    t.notification.addRecipe.message = "Votre recette a bien été créée"
                } else {
                    t.notification.addRecipe.open = true
                    t.notification.addRecipe.type = "is-danger"
                    t.notification.addRecipe.message = response.data.message
                }
            }).catch(function(error) {
                console.log(error)
                t.notification.addRecipe.open = true
                t.notification.addRecipe.type = "is-danger"
                t.notification.addRecipe.message = "NON"
            })
        },
        updateRecipe: function(id) {
            this.buttonDefault = false
            this.buttonUpdate = true
            this.recipeTitle = this.recipe.title
            this.recipeDescription = this.recipe.description
            this.recipePreparationTime = this.recipe.prep_time
            this.recipeServingAmount = this.recipe.serving_amount
            this.ingredients = this.recipe.ingredients
            this.steps = this.recipe.steps
        },
        editRecipe: function(id) {
            t = this
            console.log(id)
            requests.put('/update-recipe/'+id, {
                'id': id,
                'recipeTitle': t.recipeTitle,
                'recipeDescription': t.recipeDescription,
                'recipePreparationTime': t.recipePreparationTime,
                'recipeServingAmount': t.recipeServingAmount,
                'ingredients': t.ingredients,
                'steps': t.steps,
            }).then(function(response) {
                console.log(response)
                res = response.data.content
                t.recipe = res
                if(res) {
                    t.recipeTitle = ""
                    t.recipeDescription = ""
                    t.recipePreparationTime = 0
                    t.recipeServingAmount = 0
                    t.steps = []
                    t.ingredients = []

                    t.notification.addRecipe.open = true
                    t.notification.addRecipe.type = "is-success"
                    t.notification.addRecipe.message = "MODIFIÉ"

                } else {
                    t.notification.addRecipe.open = true
                    t.notification.addRecipe.type = "is-danger"
                    t.notification.addRecipe.message = response.data.message
                }
            }).catch(function(error) {
                console.log(error)
                t.notification.addRecipe.open = true
                t.notification.addRecipe.type = "is-danger"
                t.notification.addRecipe.message = "NON"
            })
        },
        cancelled: function() {
            this.buttonDefault = true
            this.buttonUpdate = false
            this.recipeTitle = ""
            this.recipeDescription = ""
            this.recipePreparationTime = 1
            this.recipeServingAmount = 1
            this.ingredients = ""
            this.steps = ""
        },
        deleteRecipe: function(id) {
            console.log(id)
            t = this
            requests.delete('/delete-recipe/'+id).then(function(response) {
                res = response.data
                t.recipe = res.content
                if(res) {
                    t.notification.deleteRecipe.open = true
                    t.notification.deleteRecipe.type = "is-success"
                    t.notification.deleteRecipe.message = response.data.message

                    var newRecipes = t.recipes.filter(function(i) {
                        return id != i.id;
                    });

                    t.recipes = newRecipes

                    if(t.buttonUpdate == true) {
                        t.buttonDefault = true
                        t.buttonUpdate = false
                        t.recipeTitle = ""
                        t.recipeDescription = ""
                        t.recipePreparationTime = 1
                        t.recipeServingAmount = 1
                        t.ingredients = ""
                        t.steps = ""
                    }
                    
                    requests.get('/get-recipe/' + t.recipes[0].id).then(function(response) {
                        t.recipe = response.data.content
                    })
                }
            }).catch(function(response) {
                console.log(response)
            }) 
        },
        removeIngredient: function(id) {
            var newItems = this.ingredients.filter(function(i) {
                return id != i.id;
            });
            this.ingredients = newItems;
        },
        removeStep: function(id) {
            var newItems = this.steps.filter(function(i) {
                return id != i.id;
            });
            this.steps = newItems;
        },
    },
    created: async function() {
        try {
            const response = await requests.get('/get-recipes')
            if(response.data.content) {
                res = response.data.content
                this.recipes = res
                last = res[0]
                const lastRecipe = await requests.get('/get-recipe/' + last.id)
                this.recipe = lastRecipe.data.content
            }
            else {
                this.message = response.data.message
            }
        } catch (error) {
            console.error(error);
        }
    },
});

Vue.config.devtools = true

