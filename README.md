# Devoir recettes de cuisine

Yoann Di Maria

## Installer le projet

### Modifier fichier de config *.env*
Modifier vos informations de connexion à la base de donnée.
```
DB_RECIPE_HOST=
DB_RECIPE_USER=
DB_RECIPE_PSSW=
DB_RECIPE_NAME=
DB_RECIPE_PORT=
```
Ajouter le schéma SQL à votre base de donnée :
Télécharger ce fichier ([recipes.sql](https://bitbucket.org/yoanndm/exam-recipes/downloads/recipes.sql)) ou récupérer-le depuis le repo. 

### Démarrer le serveur
Ce placer dans le dossier depuis le terminal, puis :
```bash
composer install
composer dump-autoload
php -S 127.0.0.1:8080 -t public/
```

## API

### Lister les recettes :
```
GET http:127.0.0.1/api/get-recipes
```

### Voir une recette :
```
GET http:127.0.0.1/api/get-recipe/:id
```
Remplacer :id par un id d'une recette

### Créer une recette :
```
POST http:127.0.0.1/api/create-recipe
```

####  Headers
Content-Type: application/json

#### Body
```json
{
    "recipeTitle": "",
    "recipeDescription": "",
    "recipePreparationTime": 1,
    "recipeServingAmount": 1,
    "ingredients": [
        {
            "title": ""
        }
    ],
    "steps":  [
        {
            "title": ""
        }
    ]
}
```

### Modifier une recette :
```
PUT http:127.0.0.1/api/update-recipe/:id
```
Remplacer :id par un id d'une recette

####  Headers
Content-Type: application/json

#### Body
```json
{
    "id": recipeId,
    "recipeTitle": "",
    "recipeDescription": "",
    "recipePreparationTime": 1,
    "recipeServingAmount": 1,
    "ingredients": [
        {
            "title": ""
        }
    ],
    "steps":  [
        {
            "title": ""
        }
    ]
}
```

### Supprimer une recette :
```
DELETE http:127.0.0.1/api/delete-recipe/:id
```
Remplacer :id par un id d'une recette
