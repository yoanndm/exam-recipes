SET FOREIGN_KEY_CHECKS = 0;
SET @tables = NULL;
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
  FROM information_schema.tables
  WHERE table_schema = 'cookingrecipes'; -- specify DB name here.

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;

-- table recipes
CREATE TABLE recipes (
	id INT AUTO_INCREMENT NOT NULL,
    title VARCHAR ( 512 ),
    description TEXT,
    image VARCHAR ( 1024 ),
    serving_amount INT DEFAULT '0',
    food_category VARCHAR ( 512 ),
    prep_time INT DEFAULT '0',
    cook_time INT DEFAULT '0',
    down_time INT DEFAULT '0',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    disabled BOOLEAN DEFAULT '0',
	PRIMARY KEY ( id )
);

CREATE TABLE ingredients (
    id INT AUTO_INCREMENT NOT NULL,
    recipe_id INT,
    title VARCHAR ( 512 ),
    description TEXT,
    image VARCHAR ( 1024 ),
    quantity INT DEFAULT '0',
    disabled BOOLEAN DEFAULT '0',
    PRIMARY KEY ( id ),
    FOREIGN KEY ( recipe_id ) REFERENCES recipes ( id ) ON DELETE CASCADE
);

CREATE TABLE steps (
    id INT AUTO_INCREMENT NOT NULL,
    recipe_id INT,
    title VARCHAR ( 512 ),
    description TEXT,
    disabled BOOLEAN DEFAULT '0',
    PRIMARY KEY ( id ),
    FOREIGN KEY ( recipe_id ) REFERENCES recipes ( id ) ON DELETE CASCADE
);