<?php

namespace App\Controllers;

use PDO;
use App\Models\Recipes;
use App\Contracts\RecipeControllerInterface;

class RecipeController implements RecipeControllerInterface
{
	protected $db;
	protected $container;

	public function __construct($container)
	{
		$this->db = $container;

		$this->response = [
			"status"  => 200,
			"message" => "Requête exécuté avec succès !"
		];
	}

	# Recipe retrieve method
	public function getRecipeById(int $recipeId)
	{
		$recipes = $this->db->prepare("
			SELECT *
				FROM recipes
			WHERE id = :recipeId
				AND disabled = 0;
		");

		$recipes->execute(['recipeId' => $recipeId]);

		$response = $recipes->fetchObject();

		if (!$response) {
			$this->response['status'] = 400;
			$this->response['message'] = "Cette recette n'existe pas !";
			return $this->response;
		}

		$ingredients = $this->db->prepare("
			SELECT *
				FROM ingredients
			WHERE recipe_id = :recipeId
				AND disabled = 0;
		");

		$ingredients->execute(['recipeId' => $recipeId]);

		$steps = $this->db->prepare("
			SELECT *
				FROM steps
			WHERE recipe_id = :recipeId
				AND disabled = 0;
		");

		$steps->execute(['recipeId' => $recipeId]);

		$response->{'ingredients'} = $ingredients->fetchAll(PDO::FETCH_OBJ);
		$response->{'steps'} = $steps->fetchAll(PDO::FETCH_OBJ);

		return $response;
	}

	# Retrives all recipes
	public function default()
	{
		$query = "SELECT * FROM recipes WHERE disabled = 0 ORDER BY id DESC LIMIT :maxrow";
		$recipes = $this->db->prepare($query);
		$recipes->execute(['maxrow' => 10]);

		$recipes = $recipes->fetchAll(PDO::FETCH_OBJ);

		if (count($recipes) == 0) {
			$this->response['message'] = "La liste est vide !";
			return $this->response;
		}

		if (!$recipes) {
			$this->response['status']  = 400;
			$this->response['message'] = "Une erreur est parvenue !";
			return $this->response;
		}

		$this->response['content'] = $recipes;

		return $this->response;
	}

	# Get a recipe
	public function read(int $recipeId)
	{
		$this->response['content'] = $this->getRecipeById($recipeId);

		return $this->response;
	}

	# Create a recipe
	public function create(Recipes $recipe)
	{
		$body = json_decode($recipe->getRecipe());

		if(!isset(
			$body->recipeTitle,
			$body->recipeDescription,
			$body->recipeServingAmount,
			$body->recipePreparationTime,
			$body->ingredients,
			$body->steps
		)) {
			$this->response['message'] = "Les champs obligatoires ne sont pas renseignés !";
			return $this->response;
		}

		if(
			empty($body->recipeTitle) or
			empty($body->recipeDescription) or
			empty($body->recipeServingAmount) or
			empty($body->recipePreparationTime) or
			empty($body->ingredients) or
			empty($body->steps)
		) {
			$this->response['message'] = "Des champs sont vides !";
			return $this->response;
		}
		$addRecipe = $this->db->prepare("
			INSERT INTO recipes (title, description, serving_amount, prep_time)
			VALUES (:title, :description, :serving_amount, :prep_time);
		");

		$addRecipe->execute([
			'title'				=> $body->recipeTitle,
			'description'		=> $body->recipeDescription,
			'serving_amount'	=> $body->recipeServingAmount,
			'prep_time'			=> $body->recipePreparationTime
		]);

		$recipeId = $this->db->lastInsertId();

		foreach ($body->ingredients as $key => $value) {
			$addIngredient = $this->db->prepare("
				INSERT INTO ingredients (recipe_id, title)
				VALUES (:recipeId, :title);
			");

			$addIngredient->execute([
				'recipeId'		=> $recipeId,
				'title'		 	=> $body->ingredients[$key]->title
			]);
		}

		foreach ($body->steps as $key => $value) {
			$addStep = $this->db->prepare("
				INSERT INTO steps (recipe_id, title)
				VALUES (:recipeId, :title);
			");

			$addStep->execute([
				'recipeId'		=> $recipeId,
				'title'		 	=> $body->steps[$key]->title
			]);
		}
		$this->response['content'] = $this->getRecipeById($recipeId);

		return $this->response;
	}

	# Delete a recipe
	public function delete(int $recipeId)
	{
		if(!isset($recipeId) or empty($recipeId)) {
			$this->response['status'] = "KO";
			$this->response['message'] = "Impossible de supprimer la recette";
			return $this->response;
		}

		$recipe = $this->getRecipeById($recipeId);

		if (!$recipe) {
			$this->response['status'] = "KO";
			$this->response['message'] = "Cette recette n'existe pas";
			return $this->response;
		}

		$query = "UPDATE recipes SET disabled = 1 WHERE id = :recipeId;";
		$recipe = $this->db->prepare($query);
		$recipe->execute(['recipeId' => $recipeId]);

		$response = ["status" => "OK", "message" => "Recette supprimée!"];
		return $response;
	}

	# Update a recipe
	public function update(Recipes $recipe)
	{
		$body = json_decode($recipe->getRecipe());

		if(!isset(
			$body->recipeTitle,
			$body->recipeDescription,
			$body->recipeServingAmount,
			$body->recipePreparationTime,
			$body->ingredients,
			$body->steps
		)) {
			$this->response['message'] = "Les champs obligatoires ne sont pas renseignés !";
			return $this->response;
		}

		if(
			empty($body->recipeTitle) or
			empty($body->recipeDescription) or
			empty($body->recipeServingAmount) or
			empty($body->recipePreparationTime) or
			empty($body->ingredients) or
			empty($body->steps)
		) {
			$this->response['message'] = "Des champs sont vides !";
			return $this->response;
		}
		$addRecipe = $this->db->prepare("
			UPDATE recipes SET
				title = :title,
				description = :description,
				serving_amount = :serving_amount,
				prep_time = :prep_time
			WHERE id = :recipeId;
		");

		$addRecipe->execute([
			'recipeId'			=> $body->id,
			'title'				=> $body->recipeTitle,
			'description'		=> $body->recipeDescription,
			'serving_amount'	=> $body->recipeServingAmount,
			'prep_time'			=> $body->recipePreparationTime
		]);

		foreach ($body->ingredients as $key => $value) {
			$del = $this->db->prepare("DELETE FROM ingredients WHERE recipe_id = :recipeId;");
			$del->execute(['recipeId' => $body->id]);

			$addIngredient = $this->db->prepare("
				INSERT INTO ingredients (recipe_id, title)
				VALUES (:recipeId, :title);
			");

			$addIngredient->execute([
				'recipeId'		=> $body->id,
				'title'		 	=> $body->ingredients[$key]->title
			]);
		}

		foreach ($body->steps as $key => $value) {
			$del = $this->db->prepare("DELETE FROM steps WHERE recipe_id = :recipeId;");
			$del->execute(['recipeId' => $body->id]);

			$addStep = $this->db->prepare("
				INSERT INTO steps (recipe_id, title)
				VALUES (:recipeId, :title);
			");

			$addStep->execute([
				'recipeId'		=> $body->id,
				'title'		 	=> $body->steps[$key]->title
			]);
		}
		$this->response['content'] = $this->getRecipeById($body->id);

		return $this->response;
	}
}
