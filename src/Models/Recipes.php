<?php

namespace App\Models;

use App\Contracts\RecipeInterface;

class Recipes implements RecipeInterface
{
    protected $recipe;

    public function getRecipe()
    {
        return $this->recipe;
    }

    public function setRecipe($recipe)
    {
        $this->recipe = $recipe;
    }
}