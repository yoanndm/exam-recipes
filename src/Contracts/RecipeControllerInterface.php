<?php

namespace App\Contracts;

use App\Models\Recipes;

interface RecipeControllerInterface
{	
    public function getRecipeById(int $recipeId);
    public function default();
    public function read(int $recipeId);
    public function create(Recipes $recipe);
    public function update(Recipes $recipe);
    public function delete(int $recipeId);
}