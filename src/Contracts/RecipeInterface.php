<?php

namespace App\Contracts;

interface RecipeInterface
{
	public function getRecipe();
	public function setRecipe($recipe);
}